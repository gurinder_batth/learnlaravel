<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Sign Up</title>
	  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css"></head>
<body>

<div class="container">


<form action='{{route('sign-up')}}' method="POST">

@csrf
  <div class="form-group">
    <label for="email">Email</label>
    <input type="email" class="form-control" name="email" required>
     @if($errors->has('email'))
    <span class="help-text"> {{ $errors->first('email') }}  </span>
    @endif
  </div>


  <div class="form-group">
    <label for="email">First Name</label>
    <input type="firstname" class="form-control" name="fname" required>
     @if($errors->has('fname'))
    <span class="help-text"> {{ $errors->first('fname') }}  </span>
    @endif
  </div>


  <div class="form-group">
    <label for="email">Last Name</label>
    <input type="last" class="form-control" name="lname" value="{{old('lname')}}">
     @if($errors->has('fname'))
    <span class="help-text"> {{ $errors->first('lname') }}  </span>
    @endif
  </div>


  <div class="form-group">
    <label for="email">DOB</label>
    <input type="date" class="form-control" name="dob">
  </div>

  

  <button type="submit" class="btn btn-primary">Submit</button>

</form>

</div>
</body>
</html>