<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
class signup extends Controller
{
     public function index(Request $request)
     {
     	
		$request->validate([
		   'fname' => 'required|min:3|max:12' ,
		   'lname' => 'required|min:3|max:12' ,
		   'email' => 'required|email',
		   'dob' => 'required|date' 
		] , ['email.required' => 'We need to know your e-mail address!']);
            

     }
}

